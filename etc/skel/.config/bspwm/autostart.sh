#!/bin/sh


#LAUNCHERS

xrandr --output HDMI-0 --primary --mode 1920x1080 --rate 60.00 
#xrandr --output LVDS-1 --mode 1440x900 --output TV-1 --off
nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }" &

~/.config/bspwm/polybar/launch.sh &
feh --randomize --bg-fill ~/Képek/*
#xsetroot -cursor_name left_ptr &
sxhkd &
nm-applet &
#variety &
#dropbox &
#pamac-tray &
#xfce4-power-manager &
#numlockx on &
#pasystray &
#conky -c $HOME/.config/conky/4-cpu-ET-Arcolinux-Fofo-LUA.conkyrc &
compton --config $HOME/.config/bspwm/compton.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#/usr/lib/xfce4/notifyd/xfce4-notifyd &
#megasync &
#(sleep 1 && xcompmgr) &
#tint2 &
#volumeicon &
#pcmanfm -d &
