#!/bin/sh


#LAUNCHERS

xrandr --output DP-2 --primary --mode 1920x1080 --rate 60.00 --output LVDS1 --off &
xrandr --output DP-1 --primary --mode 1920x1080 --rate 60.00 --output LVDS1 --off &

~/.config/bspwm/polybar/launch.sh &
feh --bg-scale /home/bazeeel/Képek/wall.png
#xsetroot -cursor_name left_ptr &
sxhkd &
#nm-applet &
#variety &
#dropbox &
#pamac-tray &
#xfce4-power-manager &
#numlockx on &
#pasystray &
#conky -c $HOME/.config/bspwm/system-overview &
compton --config $HOME/.config/bspwm/compton.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
#/usr/lib/xfce4/notifyd/xfce4-notifyd &
