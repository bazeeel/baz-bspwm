#!/bin/bash
set -e


echo "################################################################"
echo "####     Installing Display-server-xorg       ####"
echo "################################################################"


sudo pacman -Syu


sudo pacman -S xorg xorg-server xorg-apps xorg-xinit xterm