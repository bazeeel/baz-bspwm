# baz-bspwm
window manager

BSPWM

bspwm is a tiling window manager that represents windows as the leaves of a full binary tree.

It only responds to X events, and the messages it receives on a dedicated socket.

bspc is a program that writes messages on bspwm's socket.

bspwm doesn't handle any keyboard or pointer inputs: a third party program (e.g. sxhkd) is needed in order to translate keyboard and pointer events to bspc invocations.

The default configuration file is $XDG_CONFIG_HOME/bspwm/bspwmrc: this is simply a shell script that calls bspc.
Keyboard and pointer bindings are defined with sxhkd.
